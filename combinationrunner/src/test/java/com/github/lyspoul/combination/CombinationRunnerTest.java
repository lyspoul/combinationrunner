package com.github.lyspoul.combination;

import com.github.lyspoul.combination.CombinationRunner.DataPoint;
import com.github.lyspoul.combination.CombinationRunner.DataSet;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class CombinationRunnerTest
{
    @Test
    public void correct_number_of_runs() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(BasicCombination.class);
        final RunNotifier notifier = new RunNotifier();
        final TestFinishCounterListener counter = new TestFinishCounterListener();
        notifier.addListener(counter);

        runner.run(notifier);

        assertEquals("Wrong number of runs (combinations)", 12, counter.count);
    }

    public static class BasicCombination
    {
        @DataPoint
        public Integer data1;

        @DataSet
        public Collection<Integer> data1() {
            return Lists.newArrayList(1, 2, 3, 4);
        }

        @DataPoint
        public String data2;

        @DataSet
        public Collection<String> data2() {
            return Lists.newArrayList("One", "Two", "Three");
        }

        @Test
        public void dummyTest() throws Exception
        {
        }
    }

    public class TestFinishCounterListener extends RunListener
    {
        int count = 0;

        @Override
        public void testFinished(Description description) throws Exception
        {
            count++;
        }
    }
}


