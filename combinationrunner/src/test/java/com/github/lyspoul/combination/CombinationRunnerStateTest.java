package com.github.lyspoul.combination;

import com.github.lyspoul.combination.CombinationRunner.DataPoint;
import com.github.lyspoul.combination.CombinationRunner.DataSet;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.notification.RunNotifier;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CombinationRunnerStateTest
{
    @Test(expected = IllegalStateException.class)
    public void no_dataset_methods_throws_illegalstate() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(NonAnnotated.class);
        runner.run(new RunNotifier());
    }

    @Test(expected = IllegalStateException.class)
    public void no_datapoint_methods_throws_illegalstate() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(NoDataPoints.class);
        runner.run(new RunNotifier());
    }

    @Test(expected = IllegalStateException.class)
    public void mismatch_dataset_and_datapoint() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(DataMismatch.class);
        runner.run(new RunNotifier());
    }

    @Test(expected = IllegalStateException.class)
    public void field_assignment_type_mismatch() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(AssignmentTypeMismatch.class);
        runner.run(new RunNotifier());
    }

    @Test
    public void field_assignment_type_match() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(AssignmentTypeMatch.class);
        runner.run(new RunNotifier());
    }

    @Test
    public void named_dataset_method_in_point() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(NamedDataSetInPoint.class);
        runner.run(new RunNotifier());
    }

    @Test
    public void named_dataset_in_method() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(NamedDataSetMethod.class);
        runner.run(new RunNotifier());
    }

    @Test(expected = IllegalStateException.class)
    public void dataset_method_not_found() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(DataSetMethodNotFound.class);
        runner.run(new RunNotifier());
    }

    @Test
    public void collection_subclass_dataset_method() throws Exception
    {
        final CombinationRunner runner = new CombinationRunner(CollectionSubClassMethod.class);
        runner.run(new RunNotifier());
    }

    private class NonAnnotated
    {
    }

    public class NoDataPoints
    {
        @DataSet
        public Collection<Object> dataset1()
        {
            return Collections.emptyList();
        }
    }

    public class DataMismatch
    {
        @DataPoint
        public Object dataPoint1;

        @DataSet
        public Collection<Object> dataset1()
        {
            return Collections.emptyList();
        }

        @DataSet
        public Collection<Object> dataset2()
        {
            return Collections.emptyList();
        }
    }

    public class AssignmentTypeMismatch {

        @DataPoint
        public Integer data1;

        @DataSet
        public Collection<String> data1() {
            return Lists.newArrayList("One", "Two");
        }
    }

    public static class AssignmentTypeMatch
    {
        @DataPoint
        public Integer data1;

        @DataSet
        public Collection<Integer> data1() {
            return Lists.newArrayList(1, 2);
        }
    }

    public static class NamedDataSetInPoint {
        @DataPoint(dataSetName = "dataSetMethodName")
        public Integer data1;

        @DataSet
        public Collection<Integer> dataSetMethodName() {
            return Collections.emptyList();
        }
    }

    public static class NamedDataSetMethod {
        @DataPoint(dataSetName = "dataSetMethodName")
        public Integer data1;

        @DataSet(dataSetName = "dataSetMethodName")
        public Collection<Integer> dataMethod() {
            return Collections.emptyList();
        }
    }

    public static class DataSetMethodNotFound {
        @DataPoint(dataSetName = "nonExisting")
        public Integer data1;

        @DataSet(dataSetName = "existing")
        public Collection<Integer> dataMethod() {
            return Collections.emptyList();
        }
    }

    public static class CollectionSubClassMethod {
        @DataPoint
        public Integer data1;

        @DataSet
        public List<Integer> data1() {
            return Collections.emptyList();
        }
    }

}
