package com.github.lyspoul.combination.examples;

import com.github.lyspoul.combination.CombinationRunner;
import com.github.lyspoul.combination.CombinationRunner.DataPoint;
import com.github.lyspoul.combination.CombinationRunner.DataSet;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(CombinationRunner.class)
public class CalculatorTest
{
	@DataPoint(dataSetName = "aData")
	public int a;

	@DataPoint(dataSetName = "bData")
	public int b;

	@Test
	public void testSum() throws Exception
	{
		final Calculator calculator = new Calculator();
		final int sum = calculator.sum(a, b);
		assertEquals("Wrong sum calculated", (a + b), sum);
	}

	@Test
	public void testMultiply() throws Exception
	{
		final Calculator calculator = new Calculator();
		final int result = calculator.multiply(a, b);
		assertEquals("Wrong multiplication calculated", (a * b), result);
	}

	@DataSet
	public Collection<Integer> aData()
	{
		return Lists.newArrayList(1, 2, 3, 4, 5);
	}

	@DataSet
	public Collection<Integer> bData()
	{
		return Lists.newArrayList(6, 7, 8, 9, 10);
	}

	public class Calculator
	{
		int sum(int a, int b)
		{
			return a + b;
		}

		int multiply(int a, int b)
		{
			return a * b;
		}
	}
}
