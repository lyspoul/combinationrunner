package com.github.lyspoul.combination;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ClassUtils;
import org.junit.runner.Runner;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.Suite;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class CombinationRunner extends Suite
{
	private List<Runner> runners = Lists.newArrayList();

	private static final String METHODNAME_DEFAULT = "{methodName}";
	private static final String FIELDNAME_DEFAULT = "{fieldName}";

	private Class testClass;

	public CombinationRunner(Class<?> klass) throws InitializationError
	{
		super(klass, Collections.emptyList());
		this.testClass = klass;
		populateRunners();
	}

	@Override
	protected List<Runner> getChildren()
	{
		return runners;
	}

	private void populateRunners()
	{
		final List<Method> dataSetMethods = getDataSetMethods();
		final List<Field> dataPointFields = getDataPointFields();
		validateTestClass(dataSetMethods, dataPointFields);

		Map<Field, Method> datapointToDataset = matchFieldsAndMethods(dataPointFields, dataSetMethods);

		try
		{
			final Object dataInstance = testClass.newInstance();

			Map<Field, Collection> fieldToValues = Maps.newHashMap();
			for (Entry<Field, Method> entry : datapointToDataset.entrySet())
			{
				final Collection collection = (Collection) entry.getValue().invoke(dataInstance);
				fieldToValues.put(entry.getKey(), collection);
			}

			for (Set<FieldValue> singleTestParams : createCombinations(fieldToValues))
			{
				runners.add(new TestCombinationRunner(testClass, singleTestParams));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private Set<Set<FieldValue>> createCombinations(Map<Field, Collection> fieldToValues)
	{
		Set<Set<FieldValue>> result = Sets.newHashSet();
		List<FieldValue> allValues = Lists.newArrayList();
		for (Entry<Field, Collection> entry : fieldToValues.entrySet())
		{
			for (Object o : entry.getValue())
			{
				allValues.add(new FieldValue(entry.getKey(), o));
			}
		}
		for (FieldValue value1 : allValues)
		{
			for (FieldValue value2 : allValues)
			{
				if (!value1.equals(value2))
				{
					Set<FieldValue> set = Sets.newHashSet();
					set.add(value1);
					set.add(value2);
					result.add(set);
				}
			}
		}
		return result;
	}

	private void assignField(Object testInstance, Field field, Object value)
	{
		try
		{
			field.set(testInstance, value);
		}
		catch (IllegalAccessException e)
		{
			throw new RuntimeException(e);
		}
	}

	private Map<Field, Method> matchFieldsAndMethods(List<Field> dataPointFields, List<Method> dataSetMethods)
	{
		Map<Field, Method> result = Maps.newHashMap();

		for (Field field : dataPointFields)
		{
			boolean foundMatch = false;
			final Class<?> fieldType = field.getType();
			String fieldDataSetName = field.getAnnotation(DataPoint.class).dataSetName();
			if (fieldDataSetName.equals(FIELDNAME_DEFAULT))
			{
				fieldDataSetName = field.getName();
			}
			for (Method method : dataSetMethods)
			{
				String methodDataSetName = method.getAnnotation(DataSet.class).dataSetName();
				if (methodDataSetName.equals(METHODNAME_DEFAULT))
				{
					methodDataSetName = method.getName();
				}
				if (fieldDataSetName.equalsIgnoreCase(methodDataSetName))
				{
					ParameterizedType returnType = (ParameterizedType) method.getGenericReturnType();
					Class<?> methodReturnClass = (Class<?>) returnType.getActualTypeArguments()[0];
					if (!ClassUtils.isAssignable(methodReturnClass, fieldType, true))
					{
						throw new IllegalStateException(String.format("Method %s should return Collection<%s> to match field %s, but returns Collection<%s>", method.getName(), field.getType().getName(), field.getName(), methodReturnClass.getName()));
					}
					result.put(field, method);
					foundMatch = true;
				}
			}
			if (!foundMatch)
			{
				throw new IllegalStateException(String.format("No matching dataset method found for field %s (%s)", field.getName(), fieldType.getName()));
			}
		}

		return result;
	}

	private List<Method> getDataSetMethods()
	{
		final Method[] methods = testClass.getMethods();
		return Arrays.stream(methods).filter(method -> method.isAnnotationPresent(DataSet.class)).collect(Collectors.toList());
	}

	private List<Field> getDataPointFields()
	{
		final Field[] fields = testClass.getFields();
		return Arrays.stream(fields).filter(field -> field.isAnnotationPresent(DataPoint.class)).collect(Collectors.toList());
	}

	private void validateTestClass(List<Method> dataSetMethods, List<Field> dataPointFields)
	{
		if (dataSetMethods.isEmpty())
		{
			throw new IllegalStateException("No methods marked with @DataSet");
		}

		for (Method dataSetMethod : dataSetMethods)
		{
			if (!Collection.class.isAssignableFrom(dataSetMethod.getReturnType()))
			{
				throw new IllegalStateException(String.format("Method %s does not return a Collection", dataSetMethod.getName()));
			}
		}

		if (dataPointFields.isEmpty())
		{
			throw new IllegalStateException("No fields marked with @DataPoint");
		}

		if (dataPointFields.size() != dataSetMethods.size())
		{
			throw new IllegalStateException(String.format("Mismatch between @DataSet methods (%d) and @DataPoint fields (%d)", dataSetMethods.size(), dataPointFields.size()));
		}
	}

	private class FieldValue
	{
		private Field field;
		private Object value;

		FieldValue(Field field, Object value)
		{
			this.field = field;
			this.value = value;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj instanceof FieldValue)
			{
				final FieldValue other = (FieldValue) obj;
				return field.getName().equals(other.field.getName());
			}
			return false;
		}
	}

	private class TestCombinationRunner extends BlockJUnit4ClassRunner
	{
		private Class<?> testClass;
		private final Set<FieldValue> singleTestParams;

		TestCombinationRunner(Class<?> klass, Set<FieldValue> singleTestParams) throws InitializationError
		{
			super(klass);
			this.testClass = klass;
			this.singleTestParams = singleTestParams;
		}

		@Override
		protected Object createTest() throws Exception
		{
			final Object dataInstance = testClass.newInstance();
			for (FieldValue fieldValue : singleTestParams)
			{
				assignField(dataInstance, fieldValue.field, fieldValue.value);
			}
			return dataInstance;
		}

		@Override
		protected String testName(FrameworkMethod method)
		{
			return method.getName();
		}

		@Override
		protected String getName()
		{
			StringBuilder s = new StringBuilder(testClass.getName());
			s.append(" [");
			int count = 0;
			for (FieldValue param : singleTestParams)
			{
				if (count > 0 && count < singleTestParams.size())
				{
					s.append(", ");
				}
				s.append(param.field.getName());
				s.append("=");
				s.append(param.value.toString());
				count++;
			}
			s.append("]");
			return s.toString();
		}


	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.METHOD})
	public @interface DataSet
	{
		String dataSetName() default METHODNAME_DEFAULT;
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.FIELD})
	public @interface DataPoint
	{
		String dataSetName() default FIELDNAME_DEFAULT;
	}
}
